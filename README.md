# wogo-ios

Wogo App - Prueba de desarrollo.

En el repositorio encontrarán la carpeta del proyecto.

# instalación

# 1 
Clonar el repositorio en el directorio deseado
# 2
Importar el proyecto en la herramienta XCODE.
# 3
Iniciar el proyecto en un dispositivo iOS o simulador de XCODE
# 4
En la pantalla principal encontrará varias opciones de integración.
# a.
	Consulta de clima por ciudades predefinidas (Cali, Madrid, Dubai, Munich)
# b.
	Consulta personalizada (Por nombre de ciudad o coordenadas)
# c.
	Carga de imagen desde servicio web