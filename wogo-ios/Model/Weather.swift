//
//  Weather.swift
//  myfirstbank
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation

struct Weather: Codable {
    let name: String
    let weather: [WeatherElement]
    let coord: Coord
}
