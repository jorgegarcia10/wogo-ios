//
//  WeatherElement.swift
//  myfirstbank
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation

struct WeatherElement: Codable {
    let id: Int
    let main, weatherDescription, icon: String

    enum CodingKeys: String, CodingKey {
        case id, main
        case weatherDescription = "description"
        case icon
    }
}
