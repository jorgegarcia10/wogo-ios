//
//  WeatherViewController.swift
//  myfirstbank
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var weatherLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var latLbl: UILabel!
    @IBOutlet weak var lonLbl: UILabel!
    
    var weather: AnyObject?
    var weat = ""
    var desc = ""
    var city = ""
    var lat = ""
    var lon = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.weatherLbl.text = weat.uppercased()
        self.descriptionLbl.text = desc.uppercased()
        self.cityLbl.text = city.uppercased()
        self.latLbl.text = lat.uppercased()
        self.lonLbl.text = lon.uppercased()
    }
    

    /*
    // MARK: - Navigation
3687925
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
