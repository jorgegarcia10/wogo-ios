//
//  Coord.swift
//  wogo-ios
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation

class Coord: Codable {
    let lon, lat: Double
}
