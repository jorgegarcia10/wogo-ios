//
//  WeatherService.swift
//  myfirstbank
//
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation
import UIKit

class WeatherService {
    init(){}
    func excecuteService(url: String, completion: @escaping (Weather?, AnyObject?, Bool) -> Void) {
            let serviceUrl: String = url
            let url = URL(string: serviceUrl)
            var urlRequest = URLRequest(url: (url)!)
            urlRequest.httpMethod = "get"
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let config = URLSessionConfiguration.default
            config.timeoutIntervalForRequest = 10.0
            let session = URLSession(configuration: config)
            let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                if error != nil{
                    completion(nil, error as AnyObject, true)
                    return
                }
                if data == nil{
                    completion(nil, error as AnyObject, true)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse{
                    if httpResponse.statusCode == 200 {
                        do {
                            let decoder = JSONDecoder()
                            let weather = try decoder.decode(Weather.self, from: data!)
                            completion(weather, nil, false)
                            return
                        } catch {
                            completion(nil, error as AnyObject, true)
                            return
                        }
                    } else {
                        completion(nil, error as AnyObject, true)
                    }
                } else {
                    completion(nil, error as AnyObject, true)
                    return
                }
            })
            task.resume()
        }
    
        static func downloadImage(urlString: String, completion: @escaping(Bool, UIImage?) -> ()){
            let url = URL(string: urlString)
            guard let unwrappedUrl = url else {return}
            let request = URLRequest(url: unwrappedUrl)
            
            let session = URLSession.shared
            let task = session.dataTask(with: request){ data, response, error in
                guard let data = data, let image = UIImage(data: data) else {completion(false,nil); return}
                completion(true, image)
            }
            task.resume()
        }
    }
