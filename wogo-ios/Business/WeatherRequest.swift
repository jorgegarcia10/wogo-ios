//
//  WeatherRequest.swift
//  myfirstbank
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation
class WeatherRequest {
       
    var response: AnyObject?
    
    init() {}
    func findByCity(city: String) throws -> AnyObject{

        let group = DispatchGroup()
        group.enter()
        WeatherService().excecuteService(url: "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=d7e6204225106a26ef35751b8e152076&units=metric&lang=es") {  (data, error, issue) in
            if let data = data {
                self.response = data as AnyObject
            }
            if let error = error {
                self.response = error.localizedDescription! as AnyObject
            }
            group.leave()
        }
        group.wait()
        return self.response!;
        
    }
    
    func findByLocation(lat: String, lon: String) throws -> AnyObject{

        let group = DispatchGroup()
        group.enter()
        WeatherService().excecuteService(url: "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=63620734a4a8cd0cdc15552257aa0f79") {  (data, error, issue) in
            if let data = data {
                self.response = data as AnyObject
            }
            if let error = error {
                self.response = error.localizedDescription! as AnyObject
            }
            group.leave()
        }
        group.wait()
        return self.response!;
        
    }
}
