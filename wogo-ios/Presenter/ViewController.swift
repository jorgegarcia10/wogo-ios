//
//  ViewController.swift
//  wogo-ios
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtLat: UITextField!
    @IBOutlet weak var txtLon: UITextField!
    
    var weather: AnyObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func cityTapped(_ sender: Any) {
        if(self.txtCity.text == nil && self.txtCity.text == ""){
            displayAlert(message: "Please enter city field.")
        }
        do {
            weather = try WeatherRequest().findByCity(city: self.txtCity.text!)
            parseResults(response: weather)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func locationsTapped(_ sender: Any) {
        if(self.txtLat.text == nil && self.txtLat.text == "" &&
           self.txtLon.text == nil && self.txtLon.text == ""){
            displayAlert(message: "Please enter locations field.")
        }
        do {
            weather = try WeatherRequest().findByLocation(lat: self.txtLat.text!, lon: self.txtLon.text!)
            parseResults(response: weather)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func parseResults(response: AnyObject){
        if let parse = response as? Weather {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let weatherVC = storyboard.instantiateViewController(withIdentifier: "weatherVC") as? WeatherViewController
            weatherVC!.city = parse.name
            weatherVC!.lat = String(parse.coord.lat)
            weatherVC!.lon = String(parse.coord.lon)
            for weatherElement in parse.weather {
                weatherVC!.weat = weatherElement.main
                weatherVC!.desc = weatherElement.weatherDescription
            }
            self.present(weatherVC!, animated: true, completion: nil)
        }
    }
    
    func displayAlert(message: String) {
        let alert = UIAlertController(title: "¡Alert!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}

