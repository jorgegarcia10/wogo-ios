//
//  ImageViewController.swift
//  wogo-ios
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getImage()
    }
    
    func getImage(){
        WeatherService.downloadImage(urlString: "http://openweathermap.org/img/w/02d.png") { (success, image) in
            if success == true {
                DispatchQueue.main.async {
                    self.imgView.image = image
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
