//
//  IndexViewController.swift
//  wogo-ios
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class IndexViewController: UIViewController {

    var weather: AnyObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func caliTapped(_ sender: Any) {
        do {
            weather = try WeatherRequest().findByCity(city: "Cali")
            parseResults(response: weather)
        } catch {
            print(error)
        }
        
    }
    @IBAction func dubaiTapped(_ sender: Any) {
        do {
            weather = try WeatherRequest().findByCity(city: "Dubai")
            parseResults(response: weather)
        } catch {
            print(error)
        }
    }
    @IBAction func madridTapped(_ sender: Any) {
        do {
            weather = try WeatherRequest().findByCity(city: "Madrid")
            parseResults(response: weather)
        } catch {
            print(error)
        }
    }
    @IBAction func munichTapped(_ sender: Any) {
        do {
            weather = try WeatherRequest().findByCity(city: "Munich")
            parseResults(response: weather)
        } catch {
            print(error)
        }
    }
    
    func parseResults(response: AnyObject){
        if let parse = response as? Weather {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let weatherVC = storyboard.instantiateViewController(withIdentifier: "weatherVC") as? WeatherViewController
            weatherVC!.city = parse.name
            weatherVC!.lat = String(parse.coord.lat)
            weatherVC!.lon = String(parse.coord.lon)
            for weatherElement in parse.weather {
                weatherVC!.weat = weatherElement.main
                weatherVC!.desc = weatherElement.weatherDescription
            }
            self.present(weatherVC!, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
