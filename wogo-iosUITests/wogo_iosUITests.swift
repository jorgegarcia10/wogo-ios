//
//  wogo_iosUITests.swift
//  wogo-iosUITests
//
//  Copyright © 2019 Jorge García. All rights reserved.
//

import XCTest
@testable import wogo_ios

class wogo_iosUITests: XCTestCase {
    
    var sut: IndexViewController!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = IndexViewController()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
}
